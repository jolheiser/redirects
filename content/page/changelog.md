---
date: "2020-01-17T12:00:00+00:00"
title: "changelog"
weight: 50
toc: false
draft: false
menu: "sidebar"
goimport: "code.gitea.io/changelog git https://gitea.com/gitea/changelog"
gosource: "code.gitea.io/changelog https://gitea.com/gitea/changelog https://gitea.com/gitea/changelog/src/branch/master{/dir} https://gitea.com/gitea/changelog/src/branch/master{/dir}/{file}#L{line}"
---

# Changelog - CLI tool for changelogs


A [command line](https://gitea.com/gitea/changelog) tool to generate changelogs.
